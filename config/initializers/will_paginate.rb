require 'will_paginate/array'

if defined?(WillPaginate)
  module WillPaginate
    module ActiveRecord
      module RelationMethods
        def per(value = nil) per_page(value) end
        def total_count() count end
        def num_pages() total_pages end
      end

      module Pagination
        def paginate options
          # count reverse page
          if (page = options[:page].to_i) < 0
            per_page = options[:per_page] || self.per_page
            total = self.count
            options[:page] = [((total - 1) / per_page) + 2 + page, 1].max
          end
          super options
        end
      end      

    end
  end
end
