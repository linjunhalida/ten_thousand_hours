class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.references :user
      t.references :target
      t.integer :spend
      t.text :summary

      t.timestamps
    end
    add_index :records, :user_id
    add_index :records, :target_id
  end
end
