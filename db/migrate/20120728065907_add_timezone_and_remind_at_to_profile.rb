class AddTimezoneAndRemindAtToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :timezone, :integer, default: 8
    add_column :profiles, :remind_at, :integer, default: 8
  end
end
