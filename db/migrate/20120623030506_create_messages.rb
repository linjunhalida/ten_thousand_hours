class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :kind
      t.text :body
      t.integer :from
      t.integer :to
      t.boolean :read, default: false

      t.timestamps
    end
  end
end
