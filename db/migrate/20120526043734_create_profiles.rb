class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user
      t.references :target

      t.string :name
      t.string :location
      t.text :description

      t.integer :total_spend, default: 0

      t.timestamps
    end
    add_index :profiles, :user_id
    add_index :profiles, :target_id
  end
end
