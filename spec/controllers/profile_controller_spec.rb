require 'spec_helper'

describe ProfileController do

  describe "GET 'indexshow'" do
    it "returns http success" do
      get 'indexshow'
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit'
      response.should be_success
    end
  end

  describe "GET 'update'" do
    it "returns http success" do
      get 'update'
      response.should be_success
    end
  end

end
