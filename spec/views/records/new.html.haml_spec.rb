require 'spec_helper'

describe "records/new" do
  before(:each) do
    assign(:record, stub_model(Record,
      :user => nil,
      :target => nil,
      :spend => 1,
      :summary => "MyText"
    ).as_new_record)
  end

  it "renders new record form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => records_path, :method => "post" do
      assert_select "input#record_user", :name => "record[user]"
      assert_select "input#record_target", :name => "record[target]"
      assert_select "input#record_spend", :name => "record[spend]"
      assert_select "textarea#record_summary", :name => "record[summary]"
    end
  end
end
