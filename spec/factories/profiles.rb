# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profile do
    user nil
    target nil
    name "MyString"
    location "MyString"
    description "MyText"
  end
end
