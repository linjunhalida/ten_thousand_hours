# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    kind "MyString"
    body "MyText"
    from 1
    to 1
  end
end
