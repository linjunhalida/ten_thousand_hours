# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :activity do
    user_id 1
    item_id 1
    item_type "MyString"
    summary "MyString"
  end
end
