# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :record do
    user nil
    target nil
    spend 1
    summary "MyText"
  end
end
