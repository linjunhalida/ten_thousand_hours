def fake_data
  now = Time.now

  # targets
  ["be a piano master", "dancer", "swim", "keep thin", "english"].each do |name|
    Target.find_or_create_by_name name
  end
  t = Target.find_by_name "be a piano master"

  # create fake user
  (390..1000).each do |i|
    u = User.find_or_create_by_email "fake#{i}@10000hours.tk"
    u.password = "334000"
    u.save
    u.profile.update_attributes name: "fake#{i}", target: t

    puts "fake user: #{u.profile.name}"

    # create fake record
    day_count = rand(300) + 100
    everyday_spend = rand(10) + 1
    laziness = rand
    day_count.times.each do |before_days|
      next if rand > laziness

      spend = everyday_spend * (0.5 + rand)
      spend = [spend, 0].max
      spend = [spend, 12].min

      record_time = now - before_days.day
      summary = (rand > 0.8)? "I did a great job today." : nil
      u.records.create target: u.profile.target, spend: spend, summary: summary
    end
  end

end

fake_data
