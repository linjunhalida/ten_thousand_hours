//= require jquery
//= require jquery.transit

//= require jquery_ujs
//= require jquery.appear-1.1.1.min

//= require rails.validations
//= require jquery.event.drag-2.0.min
//= require jquery.timeago

//= require gmaps4rails/googlemaps.js
//= require jcrop

//= require jquery.pjax
//= require chosen-jquery

//= require jquery.markitup
//= require jQuery.fileinput
//= require jquery.scrollTo

