$.fn.customFileInput = function(){
	//apply events and styles for file input element
	var fileInput = $(this)
		.bind('change',function(){
			//get file name
			var fileName = $(this).val().split(/\\/).pop();
                        console.log(fileName)
			$('.upload-filename').text(fileName)
		})
		
	upload = $('.upload-btn')
        upload.hover(function(e){
			fileInput.css({
				'left': e.pageX - upload.offset().left - fileInput.outerWidth() + 20, //position right side 20px right of cursor X)
				'top': e.pageY - upload.offset().top - $(window).scrollTop() - 30
			});	
		})
		
	//return jQuery
	return $(this);
};