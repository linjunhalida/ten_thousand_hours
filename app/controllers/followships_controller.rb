class FollowshipsController < ApplicationController
  def create
    user = User.find params[:user_id]
    current_user.followings.find_or_create_by_following_user user
    render nothing: true
  end

  def index
    @followings = current_user.followings
  end
end
