class MessagesController < ApplicationController

  def index
    @messages = current_user.received_messages
    current_user.read_message!
  end

  def new
    @message = current_user.messages.new kind: params[:kind]
    render partial: "new", content_type: "text/html"
  end

  def create 
    recver = User.find params[:message][:to]
    render inline: "kind error" unless Message::KINDS.include? params[:message][:kind]
    @message = current_user.messages.new params[:message].slice(:to, :kind, :body)
    @message.save
    render nothing: true
  end
end
