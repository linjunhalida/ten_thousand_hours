class HomeController < ApplicationController
  def index
  end

  def record
    render partial: "home/record", content_type: "text/html"
  end

  def period
    from = params[:from].to_i
    to = params[:to].to_i
    return render inline: "from is bigger then to" if from > to
    return render inline: "interval too large" if to - from > 1000
    render partial: "time_period", locals: {from: from, to: to}, content_type: "text/html"
  end

  def select_user
    @user = User.find params[:user_id]
    render partial: "user_control", content_type: "text/html"
  end
  
end
