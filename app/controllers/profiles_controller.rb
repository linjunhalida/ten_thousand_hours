class ProfilesController < ApplicationController
  
  def show
    @user = current_user
    render "users/show"
  end

  def edit
    @profile = current_user.profile
  end

  def update
    @profile = current_user.profile
    respond_to do |format|
      if @profile.update_attributes(params[:profile])
        format.html { redirect_to profile_path, notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
    
  end

  def target
    profile = current_user.profile
    profile.target = Target.first
    profile.save
    redirect_to :root
  end
end
