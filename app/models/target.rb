class Target < ActiveRecord::Base
  validates_uniqueness_of :name
  attr_accessible :name, :description
end
