class Timeline
  def self.users from, to
    User.joins(:profile).where("profiles.total_spend >= ? and profiles.total_spend <= ?", from, to)
  end

  def self.milestones from, to
    from = (from/10)*10
    from = 0 if from < 0
    to = (to/10 + 1)*10
    (from..to).step 10
  end
end
