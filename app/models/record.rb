class Record < ActiveRecord::Base
  belongs_to :user
  belongs_to :target
  attr_accessible :target, :spend, :summary

  def spend
    self[:spend] || 0
  end

  def summary
    self[:summary] || ""
  end

  after_save {self.user.profile.update_total_spend}

end
