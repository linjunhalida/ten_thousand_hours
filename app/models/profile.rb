class Profile < ActiveRecord::Base
  belongs_to :user
  belongs_to :target
  attr_accessible :description, :location, :name, :target, :total_spend, :timezone, :remind_at

  def update_total_spend
    self.total_spend = self.user.records.where(target_id: self.target_id).sum(:spend)
    self.save
  end
end

