class Message < ActiveRecord::Base
  belongs_to :sender, class_name: "User", foreign_key: :from
  belongs_to :recver, class_name: "User", foreign_key: :to

  KINDS = ["up", "down"]
end
