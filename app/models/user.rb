class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body

  has_one :profile, dependent: :destroy
  has_many :records, dependent: :destroy

  has_many :messages, class_name: :Message, foreign_key: :from
  has_many :received_messages, class_name: :Message, foreign_key: :to, order: "id desc"

  has_many :followers, class_name: :Followship, foreign_key: :following_id, order: "created_at desc", dependent: :destroy
  has_many :followings, class_name: :Followship, foreign_key: :follower_id, order: "created_at desc", dependent: :destroy

  before_create do
    build_profile
    true
  end

  def name
    self.profile.name || self.email
  end

  def record_today?
    record = self.records.order("id desc").first
    return false unless record
    return false if record.updated_at < Time.now - 1.day
    return record
  end

  def last_record
    self.records.order("id desc").last
  end

  def display_scale
    total = self.profile.total_spend
    sep = 20
    {from: total - sep, to: total + sep}
  end

  def unread_messages
    self.received_messages.where(read: false)
  end

  def read_message!
    self.unread_messages.each{|m| m.update_attribute :read, true}
    # ActiveRecord::Base.connection.execute("update messages set read = True where `to` = #{self.id}")
  end

end
