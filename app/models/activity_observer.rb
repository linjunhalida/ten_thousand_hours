class ActivityObserver < ActiveRecord::Observer
  observe :message, :record, :followship

  def after_create(item)
    activity = Activity.new(item: item)
    case item.class
    when Message
      activity.user = item.sender
    when Record
      activity.user = item.user
    when Followship  
      activity.user = follower_user
    end

    activity.save
  end
end

