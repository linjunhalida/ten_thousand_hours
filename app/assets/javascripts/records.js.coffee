window.init_record_control = ->
    $(".record_control").on "click", "#btn_record_today", (e)->
        e.preventDefault()
        dlg = $("#record_dlg")
        dlg.modal('show')
        dlg.on 'ajax:success', ->
            dlg.modal('hide')
            $('.record_control').hide(300)
            $.get '/home/record', (data)->
                $('.record_control').html(data).show(300)
