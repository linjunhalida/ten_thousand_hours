window.init_timeline =(user) ->
    selected_user_id = 0
    current=user.current
    span=40

    timeline = $('#timeline')
    metric = timeline.find("#metric")
    timeline_slider=$(".timeline_slider")
    timeline_slider.val(current)
    timeline_slider.attr "title",current+" hours"
    timeline_slider.bind "mouseup",()->
        current=parseInt timeline_slider.val()
        $.ajax
            url:"/home/period"
            data:
                from: parseInt(current)-span/2
                to: parseInt(current)+span/2
            success:(data)->
                scroll_timeline(data)

    old_time_period = null
    scroll_timeline=(data)->
        old_time_period.remove() if old_time_period
        old_time_period = metric.find('.time_period')
        timeline_slider.attr "title",current+" hours"
        metric.append data
        metric.animate top:(-1)*current*10+150

    metric.css top:(-1)*current*10+150

    $(".timeline_up").click ()->
        current-= span
        current= span/2 - 10  if current < -10
        $.ajax
            url:"/home/period"
            data:
                from:current-span/2
                to:current+span/2
            success:(data)->
                scroll_timeline(data)
                timeline_slider.val(current)

    $(".timeline_down").click ()->
        current+=span
        $.ajax
            url:"/home/period"
            data:
                from:current-span/2
                to:current+span/2
            success:(data)->
                scroll_timeline(data)
                timeline_slider.val(current)

    timeline.on 'hover', '.user', ->
        self = $(this)

        hovered_user_id = parseInt(self.data("user_id"))
        return if selected_user_id == hovered_user_id
        selected_user_id = hovered_user_id
        $.get '/home/select_user', {user_id: selected_user_id}, (data)->
            $('#selected_user_outter').html(data)

    # $('#timeline .user').popover
    #     title: -> $(this).data('user_name')
    #     content: -> $(this).data('user_summary')


window.init_timeline_user_control = ()->
    self = $('#selected_user')
    user_id = self.data('user_id')

    self.on 'click', '.msg_btn', ->
        $.get '/messages/new', {kind: $(this).data("kind"), user_id: user_id}, (data)->
            self.find(".msg_form").html(data)

    self.on 'click', '.follow_btn', ->
        $.post '/followships', {user_id: user_id}, ->
            bootbox.alert("关注成功！")
    # metric.overscroll

window.init_user_statistics = (opt)->
    show_data
        container: 'week_data_container'
        title: 'week'
        data: opt.week
        start: Date.UTC(2006, 0, 1)
    show_data
        container: 'month_data_container'
        title: 'month'
        data: opt.month
        start: Date.UTC(2006, 0, 1)
    show_data
        container: 'year_data_container'
        title: 'year'
        data: opt.year
        start: Date.UTC(2006, 0, 1)

show_data = (opt)->
    chart = new Highcharts.Chart(
        chart:
            renderTo: opt.container,
            type: 'line',
            marginRight: 130,
            marginBottom: 25
        title:
            text: opt.title
        tooltip:
            shared: true
        xAxis:
            type: 'datetime',
            title:
                text: null,
        legend:
            enabled: false
        series: [{
                  type: 'area',
                  name: '小时',
                  pointInterval: 24 * 3600 * 1000,
                  pointStart: opt.start
                  data: opt.data
                }]
    )
