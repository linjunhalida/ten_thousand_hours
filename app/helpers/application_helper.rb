module ApplicationHelper
  def user_image user
    name = user.profile.name
    url = Gravatar.new(user.email).image_url(size: 40)
    image_tag url, alt: name, title: name, class: "user_image"
  end
end
