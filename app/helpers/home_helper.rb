module HomeHelper
  def user_pos user
    left = (user.email.hash % 8)*100 + 190
    left = 90 if user == current_user
    top = user.profile.total_spend * 10
    "top: #{top}px; left: #{left}px"
  end

  def user_traces user
    user.records.order('id desc').limit(1).reverse
  end

  def traces_height traces
    count = traces.map(&:spend).inject{|sum,x| sum + x }
    height = (count || 0) * 10
  end

  def user_statistic_data user
    data = {}

    {week: 7, month: 30, year: 365}.each do |k, v|
      data[k] = [0] * v
      user.records
        .where('created_at > ?', Time.now - v.days)
        .order('created_at desc')
        .each do |r|
        day = (r.created_at - (Time.now - v.days)) / (3600*24)
        data[k][day] = r.spend
      end
    end

    data
  end
end
